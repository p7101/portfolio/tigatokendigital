using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class GameUI : MonoBehaviour
{
    [Header("UI")]
    public GameObject winUI;
    public TextMeshProUGUI winText;
    public TextMeshProUGUI details;

    [Header("Components")]
    public PhotonView photonView;

    public static GameUI instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {

    }

    public void setWinUI(string winnerName, string totalJumps)
    {
        winUI.gameObject.SetActive(true);
        winText.text = winnerName + " WINS";
        details.text = "With " + totalJumps + " jumps";
    }
}
