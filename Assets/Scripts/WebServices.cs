using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;

public class WebServices : MonoBehaviour
{
    public static WebServices instance;

    static readonly HttpClient client = new HttpClient();


    public string jsonText;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        /*for (int i = 0; i < 20; i++)
        {
            DelAsync(i);
        }*/
        //PostAsync("Player 17", "99");
        //GetAsync(17);
        //DelAsync(17);
        //WebServices.instance.GetAsync(2);
        //jsonText = WebServices.instance.jsonText;
        //WinGameClass winGame = JsonConvert.DeserializeObject<WinGameClass>(jsonText);
        //Debug.Log(winGame.playerName);
    }

    public async Task PostAsync(string playerName, string playerPoint)
    {
        //HttpContent content = new StringContent("{\"playerName\":\" playerName 20\"," + "\"playerPoint\":\"98\"}", Encoding.UTF8, "application/json");
        HttpContent content = new StringContent("{\"playerName\":\"" + playerName + "\"," + "\"playerPoint\":\"" + playerPoint + "\"}", Encoding.UTF8, "application/json");
        try
        {
            HttpResponseMessage response = await client.PostAsync("https://61b964df38f69a0017ce5ffe.mockapi.io/ScoreBoard", content);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            Debug.Log(responseBody);
        }
        catch (HttpRequestException e)
        {
            Debug.Log("\nException Caught!");
            Debug.Log("Message :{0} " + e.Message);
        }
    }
    public async Task GetAsync(int id)
    {
        try
        {
            HttpResponseMessage response = await client.GetAsync("https://61b964df38f69a0017ce5ffe.mockapi.io/ScoreBoard/" + id);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            jsonText = responseBody;
            Debug.Log(responseBody);
        }
        catch (HttpRequestException e)
        {
            Debug.Log("\nException Caught!");
            Debug.Log("Message :{0} " + e.Message);
        }
    }

    public async Task DelAsync(int id)
    {
        try
        {
            HttpResponseMessage response = await client.DeleteAsync("https://61b964df38f69a0017ce5ffe.mockapi.io/ScoreBoard/" + id);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            Debug.Log(responseBody);
        }
        catch (HttpRequestException e)
        {
            Debug.Log("\nException Caught!");
            Debug.Log("Message :{0} " + e.Message);
        }
    }


}
