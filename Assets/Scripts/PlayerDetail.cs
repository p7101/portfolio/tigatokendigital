using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

public class PlayerDetail : MonoBehaviourPunCallbacks
{
    [HideInInspector]
    public int id;
    public int jump;
    public int playerPoint;
    public TextMeshProUGUI playerName;

    public static PlayerDetail instance;
    private void Awake()
    {
        instance = this;
    }

    [PunRPC]
    public void JumpEverySec()
    {
        InvokeRepeating("Jump", 0f, 1f);
    }

    private void Jump()
    {
        jump = Random.Range(1,3);
        playerPoint += jump;
        Debug.Log("Player " + id + " Counter: " + playerPoint);
    }
}
