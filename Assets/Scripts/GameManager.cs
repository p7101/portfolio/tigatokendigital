using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using Newtonsoft.Json;

public class GameManager : MonoBehaviourPunCallbacks
{
    [Header("Players")]
    public GameObject[] players;
    public float[] jumps;
    public int[] counters;


    private string jsonText;
    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("InitializePlayer", RpcTarget.MasterClient);
            photonView.RPC("StartJump", RpcTarget.MasterClient);
            InvokeRepeating("StartAnimJump", 0f, 1f);
            InvokeRepeating("WinGame", 10f, 10f);
        }

        InvokeRepeating("PhotonWinGame", 13f, 10f);
    }

    void Update()
    {

    }

    [PunRPC]
    void InitializePlayer()
    {
        for(int i=0; i<4; i++)
        {
            PlayerDetail playerScript = players[i].GetComponent<PlayerDetail>();
            playerScript.id = i + 1;
        }
    }

    [PunRPC]
    void StartJump()
    {
        for (int i = 0; i < 4; i++)
        {
            players[i].GetComponent<PlayerDetail>().JumpEverySec();
        }
    }

    void StartAnimJump()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("AnimJump", RpcTarget.MasterClient);
        }
    }

    [PunRPC]
    void AnimJump()
    {
        StartCoroutine(AnimJumpEnum());
    }

    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {
        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.position = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }
    }
    IEnumerator AnimJumpEnum()
    {
        for (int i = 0; i < 4; i++)
        {
            jumps[i] = (float)players[i].GetComponent<PlayerDetail>().jump;
            var seconds = Time.deltaTime / 1f / jumps[i] / 2f;

            StartCoroutine(MoveToPosition(players[i].transform, new Vector3(players[i].transform.position.x, 2.4f, players[i].transform.position.z), 1f / jumps[i] / 2f));
            yield return new WaitForSeconds(seconds);
            StartCoroutine(MoveToPosition(players[i].transform, new Vector3(players[i].transform.position.x, 0.5f, players[i].transform.position.z), 1f / jumps[i] / 2f));
            yield return new WaitForSeconds(seconds);
        }
    }









    IEnumerator WaitToResetWinGame(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        photonView.RPC("ResetWinGame", RpcTarget.AllBuffered);
    }

    IEnumerator WaitToShowUI(float seconds, int id)
    {
        yield return new WaitForSeconds(seconds);
        WebServices.instance.GetAsync(id);
        jsonText = WebServices.instance.jsonText;
        WinGameClass winGame = JsonConvert.DeserializeObject<WinGameClass>(jsonText);

        GameUI.instance.setWinUI(winGame.playerName, winGame.playerPoint);
    }

    void PhotonWinGame()
    {
        
        int id = 3;
        StartCoroutine(WaitToShowUI(3, id));
        //WebServices.instance.GetAsync(id);
        //jsonText = WebServices.instance.jsonText;
        //WinGameClass winGame = JsonConvert.DeserializeObject<WinGameClass>(jsonText);
        //
        //GameUI.instance.setWinUI(winGame.playerName, winGame.playerPoint);

        id++;


        /*"if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("WinGame", RpcTarget.AllBuffered);
            StartCoroutine(WaitToResetWinGame(5f));
        }*/
    }

    [PunRPC]
    void WinGame()
    {
        var max = 0;
        var maxId = 0;
        for (int i = 0; i < 4; i++)
        {
            counters[i] = players[i].GetComponent<PlayerDetail>().playerPoint;
            if (max < counters[i])
            {
                max = counters[i];
                maxId = i;
            }
        }

        string winPlayerName = players[maxId].GetComponent<PlayerDetail>().playerName.text;
        string winPlayerPoint = players[maxId].GetComponent<PlayerDetail>().playerPoint.ToString();

        WebServices.instance.PostAsync(winPlayerName, winPlayerPoint);
        //GameUI.instance.setWinUI("PLAYER " + players[maxId].GetComponent<PlayerDetail>().id, max);
    }
    
    [PunRPC]
    void ResetWinGame()
    {
        GameUI.instance.winUI.SetActive(false);
    }

    void ResetCounter()
    {
        for (int i = 0; i < 4; i++)
        {
            players[i].GetComponent<PlayerDetail>().playerPoint = 0;
        }
    }
}

public class WinGameClass
{
    public string playerName;
    public string playerPoint;
}